// Get review Data / Retrieve Function
fetch("https://movie-reviews-api-u285.onrender.com/reviews")
    .then(response => response.json())
    .then(data => showReviews(data));

// VIEW REVIEWS - used to display each review from JSON placeholder.
const showReviews = (reviews) => {
    //Create a variable that will contain all the review
    let reviewEntries = "";

    reviews.forEach((review) => {
        reviewEntries += `
            <div id="review-${review._id}">
                <h3 id="review-title-${review._id}">${review.title}</h3>
                <h4 id="review-rating-${review._id}">Rating: ${review.rating}</h4>
                <p id="review-body-${review._id}"><strong>Review: </strong>${review.review}</p>
                <button onClick="editReview('${review._id}'
                )">Edit</button>
                <button onClick="deleteReview('${review._id}'
                )">Delete</button>
            </div>
        `;
    });
    // To assign the value of reviewEntries to the element with "div-review-entries" id.
    document.querySelector("#div-review-entries").innerHTML = reviewEntries
};

// ADD REVIEW - used to add review
document.querySelector("#form-add-review").addEventListener("submit", e => {
    e.preventDefault();

    fetch("https://movie-reviews-api-u285.onrender.com/reviews", {
       method: "POST",
       body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            rating: document.querySelector("#txt-rating").value,
            review: document.querySelector("#txt-review").value,
            userId: 1
       }),
       headers: {
            "Content-Type": "application/json"
       }
    }).then(response => response.json()).then(data => {
        alert("Post Successfully Added!");

        fetch("https://movie-reviews-api-u285.onrender.com/reviews")
        .then(response => response.json())
        .then(data => showReviews(data));
    });
    
    document.querySelector("#txt-title").value = null;
    document.querySelector("#txt-rating").value = null;
    document.querySelector("#txt-review").value = null;

});

// UPDATE REVIEW - used to update review
const editReview = (id) => {

    // Displayed Post in the Post Section (Source of data)
    let title = document.querySelector(`#review-title-${id}`).innerHTML;
    let rating = document.querySelector(`#review-rating-${id}`).innerHTML.substring(8);
    let review = document.querySelector(`#review-body-${id}`).innerHTML.substring(25);

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-rating").value = rating;
    document.querySelector("#txt-edit-review").value = review;

    document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

document.querySelector("#form-edit-review").addEventListener("submit", e => {
    e.preventDefault();

    let id = document.querySelector("#txt-edit-id").value;

    fetch("https://movie-reviews-api-u285.onrender.com/reviews", {

        method: "PUT",
        body: JSON.stringify({
            _id: id,
            title: document.querySelector("#txt-edit-title").value,
            rating: document.querySelector("#txt-edit-rating").value,
            review: document.querySelector("#txt-edit-review").value,
            userId: 1
        }),
        headers: {
            "Content-type": "application/json"
        }
    }).then(res => res.json())
        .then(data => {
            alert("Review Successfully Updated");

        fetch("https://movie-reviews-api-u285.onrender.com/reviews")
        .then(response => response.json())
        .then(data => showReviews(data));
    });

    document.querySelector("#txt-edit-title").value = null;
    document.querySelector("#txt-edit-rating").value = null;
    document.querySelector("#txt-edit-review").value = null;

    document.querySelector("#btn-submit-update").setAttribute("disabled", true);
});

// DELETE REVIEW - used to delete a review
const deleteReview = (id) => {
    fetch(`https://movie-reviews-api-u285.onrender.com/reviews`, {
        method: "DELETE",
        body: JSON.stringify({
            _id: id,
        }),
        headers: {
            "Content-type": "application/json"
        }
    }).then(data => {
        alert("Review Successfully Deleted");

        fetch("https://movie-reviews-api-u285.onrender.com/reviews")
        .then(response => response.json())
        .then(data => showReviews(data));
    });
};