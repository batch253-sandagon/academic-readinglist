// Object Contructor
function Character(name, classType, level){
    this.name = name;
    this.classType = classType;
    this.level = level;
    this.health = level * 2;
    this.hasDodged = false;
    this.bag = [];
    this.dodge = function(){
        console.log(`${this.name} dodged the attack!`);
        this.hasDodged = true;
    }
    this.faint = function(){
        console.log(`${this.name} has fainted`);
    }
    this.attack = function(monster){
        console.log(`${this.name} attacked the ${monster.type}!`);
        monster.takeDamage(this);
    }
};

// Monster Object
const monster = {
    type: "Dragon",
    level: 10,
  
    attack: function(character) {
        if (character.health === 2 && character.hasDodged === false) {
            console.log(`The ${this.type} attacked ${character.name}!`);
            character.dodge();
        } 
        
        if (character.health > 0) {
            character.health -= 2;
            console.log(`The ${this.type} attacked ${character.name}!`);
            console.log(`${character.name}'s health is now reduced to ${character.health}`);
        } else {
          character.faint();
        }
    },
  
    takeDamage: function(character) {
      console.log(`The ${this.type} takes a hit and loses ${character.level} health!`);
    }
};

// Merchant Object
const merchant = {
    items: ["Sword", "Potion", "Shield"],
  
    buy: function(item, character) {
      character.bag.push(item);
      console.log(`${character.name} bought a ${item}!`);
    },
  
    sell: function (item, character) {
      const index = character.bag.indexOf(item);
      if (index !== -1) {
        character.bag.splice(index, 1);
        console.log(`${character.name} sold a ${item}!`);
      } else {
        console.log(`${character.name} does not have a ${item}!`);
      }
    }
};

// const zed = new Character("Zed", "Assassin", 2)
// console.log(zed);
// merchant.buy("Sword", zed)
// merchant.buy("Shield", zed)
// console.log(zed.bag);
// merchant.sell("Shield", zed)
// console.log(zed.bag);
// zed.attack(monster)
// monster.attack(zed);
// zed.attack(monster)
// monster.attack(zed);
// monster.attack(zed);