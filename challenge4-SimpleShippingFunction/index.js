function shipping(itemWeight, shippingMethod){
    let total = 0;

    if(itemWeight < 0.5){
        total = 90;
    } else if (itemWeight > 0.5 && itemWeight <= 1){
        total = 120;
    } else if (itemWeight > 1 && itemWeight <= 3){
        total = 250;
    } else if (itemWeight > 3 && itemWeight <= 5){
        total = 380;
    } else if (itemWeight > 5 && itemWeight <= 10){
        total = 550;
    } else {
        return "The weight of your item/s has exceeded the limit."
    };

    switch(shippingMethod) {
        case "local":
            total *= 1.0;
            break;
        case "overseas":
            total *= 1.5;
            break;
        default:
            return "Provide a valid shipping method"
    }

    return `The total amount of your shipping fee is ₱${total}`
}

console.log(shipping(12, "overseas"));
console.log(shipping(5, "basta"));
console.log(shipping(5, "overseas"));